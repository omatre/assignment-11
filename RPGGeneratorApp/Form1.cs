﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CharacterLibrary;
using CsvHelper;
using System.Data.SqlClient;

namespace RPGGeneratorApp
{
    public partial class Form1 : Form
    {
        private List<Character> Characters { get; set; }
        private List<Type> leafclasses;

        //On form creation
        public Form1()
        {
            InitializeComponent();
            Characters = new List<Character>();
            Characters = GetAllCharacters();
            leafclasses = FindAllLeafClasses();
            FillClassComboBox();
        }

        /// <summary>
        /// This method makes it possible to add new classes to CharacterLibrary.dll 
        /// without having to change this code.
        /// </summary>
        /// <returns>All non-abstract class-types from CharacterLibrary.dll</returns>
        private List<Type> FindAllLeafClasses()
        {
            var output = new List<Type>();

            output = Assembly.GetAssembly(typeof(Character))
                .GetTypes()
                .Where(t =>
                {
                    return !t.GetTypeInfo().IsAbstract;
                })
                .ToList<Type>();

            return output;
        }

        /// <summary>
        /// Fills the Type-combobox with available types from CharacterLibrary.dll.
        /// </summary>
        private void FillClassComboBox()
        {
            CbClasses.Items.Add("");
            foreach (Type item in leafclasses)
            {
                CbClasses.Items.Add(item.Name);
            }
        }

        /// <summary>
        /// Creates a new character using the user inputs, adds it to the character-list
        /// and saves it to the database, when button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddButtonClicked(object sender, EventArgs e)
        {
            Character newCharacter = null;

            try
            {
                //Instantiating the added object
                foreach (Type item in leafclasses)
                {
                    if (item.Name == (string)CbClasses.SelectedItem)
                    {
                        newCharacter = Activator.CreateInstance(item, TbName.Text, (int)NudHP.Value,
                            (int)NudEnergy.Value, (int)NudArmerRating.Value) as Character;
                    }
                }

                if (newCharacter != null)
                {
                    //Ensure no character with same name
                    if (Characters.Any(t => t.Name == newCharacter.Name))
                    {
                        throw new Exception("Character with same name already exists");
                    }

                    //Prints out text in summary label
                    Summary.Text = $"Summary:\n\n{ newCharacter.Name } is a { newCharacter.GetType().Name.ToString() }.\n\n" +
                        $"HP: { newCharacter.HP }\n" +
                        $"Energy: { newCharacter.Energy }\n" +
                        $"Armor Rating: { newCharacter.ArmorRating }\n\n" +
                        $"When Attacking: { newCharacter.Attack() }\n" +
                        $"When Moving: { newCharacter.Move() }";

                    Characters.Add(newCharacter);
                }
                else
                {
                    throw new NullReferenceException();
                }
            }
            catch (NullReferenceException)
            {
                Summary.Text += "\nError: New character not created";
            }
            catch (Exception ex)
            {
                Summary.Text += "\nError: " + ex.Message;
            }

            //Save to DB
            try
            {
                SaveToDB(newCharacter);
            }
            catch (Exception ex)
            {
                Summary.Text += "\nError: " + ex.Message;
            }

            ClearInput();
        }

        /// <summary>
        /// Clear inputs in form
        /// </summary>
        private void ClearInput()
        {
            CbClasses.SelectedItem = CbClasses.Items[0];
            TbName.Text = "";
            NudHP.Value = 0;
            NudEnergy.Value = 0;
            NudArmerRating.Value = 0;
        }

        /// <summary>
        /// Saves to CSV-file, Excel-format when button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveButtonClicked(object sender, EventArgs e)
        {
            const string filename = @"Characters.csv";

            try
            {
                using (StreamWriter writer = new StreamWriter(filename))
                {
                    using (CsvWriter csv = new CsvWriter(writer, CultureInfo.CurrentCulture))
                    {
                        csv.WriteHeader<Character>();   //Property names writen as headers
                        csv.NextRecord();               //Flushes the output
                        csv.WriteRecords<Character>(Characters); //Writes all the characters to file
                    }
                }

                Summary.Text = $"Characters saved to { Path.GetFullPath(filename) }";
            }
            catch (Exception ex)
            {
                Summary.Text += "\nError: " + ex.Message;
            }
        }

        /// <summary>
        /// Save character to database.
        /// </summary>
        /// <param name="character">Instance to save to DB</param>
        private void SaveToDB(Character character)
        {
            string connectionString = @"Server=PC7596\SQLEXPRESS;"
                                    + @"Database=RPGGenerator;"
                                    + @"Trusted_Connection=True";
            string sql = "INSERT INTO Characters (Name, HP, Energy, ArmorRating, Type) " +
                "VALUES (@Name, @HP, @Energy, @ArmorRating, @Type)";

            var builder = new SqlConnectionStringBuilder(connectionString);

            using (var connection = new SqlConnection(builder.ConnectionString))
            {
                connection.Open();

                using (var command = new SqlCommand(sql, connection))
                {
                    command.Parameters.AddWithValue("@Name", character.Name);
                    command.Parameters.AddWithValue("@HP", character.HP);
                    command.Parameters.AddWithValue("@Energy", character.Energy);
                    command.Parameters.AddWithValue("@ArmorRating", character.ArmorRating);
                    command.Parameters.AddWithValue("@Type", character.GetType().Name);

                    command.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Reads all characters from database.
        /// </summary>
        /// <returns>A list of characters read from DB</returns>
        private List<Character> GetAllCharacters()
        {
            var output = new List<Character>();

            string connectionString = @"Server=PC7596\SQLEXPRESS;"
                                    + @"Database=RPGGenerator;"
                                    + @"Trusted_Connection=True";
            string sql = "SELECT * FROM Characters";

            var builder = new SqlConnectionStringBuilder(connectionString);

            try
            {
                using (var connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Character temp = Activator.CreateInstance(typeof(Character).Assembly.GetType("CharacterLibrary." + reader.GetString(5))
                                    , reader.GetString(1), reader.GetInt32(2),
                                    reader.GetInt32(3), reader.GetInt32(4)) as Character;
                                output.Add(temp);
                            }
                        }

                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                Summary.Text += "\nError: " + ex.Message;
            }

            return output;
        }

        /// <summary>
        /// Prints out all characters from character-list att button click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadAllCharacters(object sender, EventArgs e)
        {
            Summary.Text = $"Summary:\n\n" +
                           $"Name | HP | Energy | Armor Rating | Type\n";
            foreach (var character in Characters)
            {
                Summary.Text += $"{ character.Name } | { character.HP } | " +
                                $"{ character.Energy } | { character.ArmorRating } | " +
                                $"{ character.GetType().Name }\n";
            }
        }

        /// <summary>
        /// Delete character selected from character-combobox from both
        /// DB and character-list at button click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteCharacterClick(object sender, EventArgs e)
        {
            string deleteString = AddedCharactersCB.SelectedItem.ToString();

            AddedCharactersCB.SelectedIndex = 0;

            Character deleteCharacter = Characters.Where(t => t.Name == deleteString)
                                                  .FirstOrDefault();

            if (deleteCharacter != null)
            {
                string connectionString = @"Server=PC7596\SQLEXPRESS;"
                                    + @"Database=RPGGenerator;"
                                    + @"Trusted_Connection=True";
                string sql = "DELETE FROM Characters WHERE Name=@Name";

                var builder = new SqlConnectionStringBuilder(connectionString);

                try
                {
                    using (var connection = new SqlConnection(builder.ConnectionString))
                    {
                        connection.Open();

                        using (var command = new SqlCommand(sql, connection))
                        {
                            command.Parameters.AddWithValue("@Name", deleteString);
                            command.ExecuteNonQuery();
                        }
                    }

                    Characters.Remove(deleteCharacter);

                    Summary.Text = $"\nSummary: { deleteCharacter.Name } deleted!";
                }
                catch (Exception ex)
                {
                    Summary.Text += "\nError: " + ex.Message;
                }
            }
        }

        /// <summary>
        /// Updates info from form inputs to character selected with character-combobox in DB,
        /// clears input form and updates character-list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateCharacterClick(object sender, EventArgs e)
        {
            string connectionString = @"Server=PC7596\SQLEXPRESS;"
                                + @"Database=RPGGenerator;"
                                + @"Trusted_Connection=True";
            string sql = "UPDATE Characters " +
                         "SET Name=@Name, HP=@HP, Energy=@Energy, ArmorRating=@ArmorRating " +
                         "WHERE Name=@Selected";

            var builder = new SqlConnectionStringBuilder(connectionString);

            try
            {
                using (var connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@Name", TbName.Text);
                        command.Parameters.AddWithValue("@HP", NudHP.Value);
                        command.Parameters.AddWithValue("@Energy", NudEnergy.Value);
                        command.Parameters.AddWithValue("@ArmorRating", NudArmerRating.Value);
                        command.Parameters.AddWithValue("@Selected", AddedCharactersCB.SelectedItem.ToString());
                        command.ExecuteNonQuery();
                    }
                }

                Summary.Text = $"\nSummary:\n\n" +
                    $"{ AddedCharactersCB.SelectedItem.ToString() } has been updated to\n\n" +
                    $"{ TbName.Text } with a HP, Energy and Armor Rating of { NudHP.Value }, { NudEnergy.Value } and { NudArmerRating.Value }!";

                ClearInput();
                Characters = GetAllCharacters();
                AddedCharactersCB.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Summary.Text += "\nError: " + ex.Message;
            }
        }

        /// <summary>
        /// Updates combobox with current character-list at button click.
        /// </summary>
        private void UpdateComboBoxClick(object sender, EventArgs e)
        {
            var characterNames = from character in Characters
                                 select character.Name;
            AddedCharactersCB.Items.Clear();
            AddedCharactersCB.Items.Add("");
            AddedCharactersCB.Items.AddRange(characterNames.ToArray());
        }
    }
}
