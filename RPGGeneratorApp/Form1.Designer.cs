﻿namespace RPGGeneratorApp
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.CbClasses = new System.Windows.Forms.ComboBox();
            this.TbName = new System.Windows.Forms.TextBox();
            this.NudHP = new System.Windows.Forms.NumericUpDown();
            this.NudEnergy = new System.Windows.Forms.NumericUpDown();
            this.NudArmerRating = new System.Windows.Forms.NumericUpDown();
            this.BtnAdd = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.Summary = new System.Windows.Forms.Label();
            this.LoadBtn = new System.Windows.Forms.Button();
            this.DeleteBtn = new System.Windows.Forms.Button();
            this.UpdateBtn = new System.Windows.Forms.Button();
            this.AddedCharactersCB = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.NudHP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NudEnergy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NudArmerRating)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Type:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "HP:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 138);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Energy:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 176);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "Armor Rating:";
            // 
            // CbClasses
            // 
            this.CbClasses.FormattingEnabled = true;
            this.CbClasses.Location = new System.Drawing.Point(138, 12);
            this.CbClasses.Name = "CbClasses";
            this.CbClasses.Size = new System.Drawing.Size(151, 28);
            this.CbClasses.TabIndex = 5;
            // 
            // TbName
            // 
            this.TbName.Location = new System.Drawing.Point(138, 54);
            this.TbName.Name = "TbName";
            this.TbName.Size = new System.Drawing.Size(125, 27);
            this.TbName.TabIndex = 6;
            // 
            // NudHP
            // 
            this.NudHP.Location = new System.Drawing.Point(138, 94);
            this.NudHP.Name = "NudHP";
            this.NudHP.Size = new System.Drawing.Size(150, 27);
            this.NudHP.TabIndex = 7;
            // 
            // NudEnergy
            // 
            this.NudEnergy.Location = new System.Drawing.Point(138, 136);
            this.NudEnergy.Name = "NudEnergy";
            this.NudEnergy.Size = new System.Drawing.Size(150, 27);
            this.NudEnergy.TabIndex = 8;
            // 
            // NudArmerRating
            // 
            this.NudArmerRating.Location = new System.Drawing.Point(138, 174);
            this.NudArmerRating.Name = "NudArmerRating";
            this.NudArmerRating.Size = new System.Drawing.Size(150, 27);
            this.NudArmerRating.TabIndex = 9;
            // 
            // BtnAdd
            // 
            this.BtnAdd.Location = new System.Drawing.Point(12, 207);
            this.BtnAdd.Name = "BtnAdd";
            this.BtnAdd.Size = new System.Drawing.Size(101, 53);
            this.BtnAdd.TabIndex = 10;
            this.BtnAdd.Text = "Save To Database";
            this.BtnAdd.UseVisualStyleBackColor = true;
            this.BtnAdd.Click += new System.EventHandler(this.AddButtonClicked);
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(119, 207);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(101, 53);
            this.BtnSave.TabIndex = 11;
            this.BtnSave.Text = "Save To CSV";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.SaveButtonClicked);
            // 
            // Summary
            // 
            this.Summary.AutoSize = true;
            this.Summary.Location = new System.Drawing.Point(330, 12);
            this.Summary.Name = "Summary";
            this.Summary.Size = new System.Drawing.Size(74, 20);
            this.Summary.TabIndex = 12;
            this.Summary.Text = "Summary:";
            // 
            // LoadBtn
            // 
            this.LoadBtn.Location = new System.Drawing.Point(226, 208);
            this.LoadBtn.Name = "LoadBtn";
            this.LoadBtn.Size = new System.Drawing.Size(101, 52);
            this.LoadBtn.TabIndex = 13;
            this.LoadBtn.Text = "Load All";
            this.LoadBtn.UseVisualStyleBackColor = true;
            this.LoadBtn.Click += new System.EventHandler(this.LoadAllCharacters);
            // 
            // DeleteBtn
            // 
            this.DeleteBtn.Location = new System.Drawing.Point(226, 349);
            this.DeleteBtn.Name = "DeleteBtn";
            this.DeleteBtn.Size = new System.Drawing.Size(101, 52);
            this.DeleteBtn.TabIndex = 13;
            this.DeleteBtn.Text = "Delete";
            this.DeleteBtn.UseVisualStyleBackColor = true;
            this.DeleteBtn.Click += new System.EventHandler(this.DeleteCharacterClick);
            // 
            // UpdateBtn
            // 
            this.UpdateBtn.Location = new System.Drawing.Point(226, 278);
            this.UpdateBtn.Name = "UpdateBtn";
            this.UpdateBtn.Size = new System.Drawing.Size(101, 52);
            this.UpdateBtn.TabIndex = 13;
            this.UpdateBtn.Text = "Update";
            this.UpdateBtn.UseVisualStyleBackColor = true;
            this.UpdateBtn.Click += new System.EventHandler(this.UpdateCharacterClick);
            // 
            // AddedCharactersCB
            // 
            this.AddedCharactersCB.FormattingEnabled = true;
            this.AddedCharactersCB.Location = new System.Drawing.Point(12, 278);
            this.AddedCharactersCB.Name = "AddedCharactersCB";
            this.AddedCharactersCB.Size = new System.Drawing.Size(151, 28);
            this.AddedCharactersCB.TabIndex = 14;
            this.AddedCharactersCB.Click += new System.EventHandler(this.UpdateComboBoxClick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(833, 444);
            this.Controls.Add(this.AddedCharactersCB);
            this.Controls.Add(this.UpdateBtn);
            this.Controls.Add(this.DeleteBtn);
            this.Controls.Add(this.LoadBtn);
            this.Controls.Add(this.Summary);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.BtnAdd);
            this.Controls.Add(this.NudArmerRating);
            this.Controls.Add(this.NudEnergy);
            this.Controls.Add(this.NudHP);
            this.Controls.Add(this.TbName);
            this.Controls.Add(this.CbClasses);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.NudHP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NudEnergy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NudArmerRating)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox CbClasses;
        private System.Windows.Forms.TextBox TbName;
        private System.Windows.Forms.NumericUpDown NudHP;
        private System.Windows.Forms.NumericUpDown NudEnergy;
        private System.Windows.Forms.NumericUpDown NudArmerRating;
        private System.Windows.Forms.Button BtnAdd;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.Label Summary;
        private System.Windows.Forms.Button LoadBtn;
        private System.Windows.Forms.Button DeleteBtn;
        private System.Windows.Forms.Button UpdateBtn;
        private System.Windows.Forms.ComboBox AddedCharactersCB;
    }
}

